
;--------- my own constants --------------
NULL = 0
TRUE = 1
FALSE = 0


HANDLE TEXTEQU <DWORD>		; used in Win32 API documentation
; Win32 Console handles
STD_INPUT_HANDLE EQU -10
STD_OUTPUT_HANDLE EQU -11		; predefined Win API constant
STD_ERROR_HANDLE EQU -12


;--------- stack --------------
;struct sqStack {
;	int *top;  //sp
;	int *base; //ss
;	int len; //size count by itemStack
;}SqStack;

sqStack struct
	top dword ? ;sp
	base dword ? ;ss
	len dword 512 ;count
sqStack ends

;--------- queue --------------
;struct queue{
;	int front;
;	int rear;
;	void* ary; //point to array hold queue items
;	unsigned int len; //array size
;};
queue struct
	front dword  ?
	rear dword ?
	ary dword ? ;array
	len dword ? ;size count by itemStack
queue ends
