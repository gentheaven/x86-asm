#include <stdio.h>
#include <string.h>

//c call asm
extern void asm_main(void);
extern void testStack(void);

struct queue {
	unsigned int front;
	unsigned int rear;
	void* ary; //point to array hold queue items
	unsigned int len; //array size
};

//maxLen <= 255, max saved 254 items
extern void initQueue(struct queue* q, void* ary, unsigned int maxLen);
extern int queueFull(struct queue* q);
extern int queueEmpty(struct queue* q);
extern int enQueue(struct queue* q, unsigned int data);
extern int deQueue(struct queue* q, unsigned int* data);

unsigned int array_queue[20];

void test_queue(void)
{
	struct queue q;
	unsigned int len, i, data = 0;
	int ret;

	printf("\nstart to test queue \n");

	len = sizeof(array_queue) / sizeof(unsigned int);
	initQueue(&q, (void*)array_queue, len); //compile will add "ret 12"
	ret = queueEmpty(&q);
	ret = queueFull(&q);

	printf("enque data:   ");
	for (i = 1; i <= len; i++) {
		ret = enQueue(&q, i);
		if (-1 == ret)
			break;
		printf("%d ", i);
	}
	printf("\ndequeue data: ");

	while(1){
		ret = deQueue(&q, &data);
		if (-1 == ret)
			break;
		printf("%d ", data);
	}
	printf("\n\n");

}

int main(int argc, char* argv)
{
	asm_main();
	//testStack();
	test_queue();
	return 0;
}