; ----------------------------------------------------------------------------
; helloworld.asm
;
; This is a Win32 console program that writes "Hello, World" on one line and
; then exits.  It needs to be linked with a C library.
; ----------------------------------------------------------------------------
 [bits 32]


;===========================================================
section .data   ; Initialized data segment
	Message        db "Hello World", 0Dh, 0Ah
	MessageLength  EQU $-Message  ; Address of this line ($) - address of Message

section .bss                                    ; Uninitialized data segment
alignb 8
	StandardHandle resq 1
	Written        resq 1


;===========================================================
section .text
;test code
;u32 calc_sectors(u32 bytes)
;input eax = bytes
;output eax = sectors
calc_sectors:
	add eax, 511
	shr eax, 9; eax / 512
ret

%macro calc_sector 0
	add eax, 511
	shr eax, 9
%endmacro
;===========================================================

	global	main

	section .text

	extern initStack
	extern stackFull
	extern stackEmpty
	extern pushdata
	extern popdata
	extern testStack
	extern _printf

main:
	xor eax, eax

	mov eax, 1023
	;call calc_sectors
	calc_sector

	push Message
	call _printf
	add esp, 4

	call testStack

	ret
