
 %include "algo-types.inc"	; 常量, 宏, 以及一些说明
 %include "c32.mac"

 [bits 32]
section .text
	proc    example_proc32 
;This defines _proc32 to be a procedure taking two arguments, 
;the first (i) an integer and the second (j) a pointer to an integer. 
;It returns i + *j.
%$i     arg 
%$j     arg 
        mov     eax,[ebp + %$i] 
        mov     ebx,[ebp + %$j] 
        add     eax,[ebx] 

endproc

;void initStack(struct sqStack s, int *ary, int len)
proc    initStack
;uses eax esi,
%$s     arg 
%$ary   arg 
%$len	arg

push eax
push esi
	mov esi, [ebp + %$s]
	mov eax, [ebp + %$ary]
	mov [esi + sqStack.base], eax
	mov [esi + sqStack.top], eax
	mov eax, [ebp + %$len]
	mov [esi + sqStack.len], eax
pop esi
pop eax

endproc

;int stackFull(struct sqStack *s)
;return: eax = 1 if full; 0 if not full
proc stackFull
;uses esi ebx,
%$s     arg

push ebx
push esi
	mov eax, 1
	mov esi, [ebp + %$s]
	;if((s->top - s->base) >= s->len) {
	mov ebx, [esi + sqStack.top]
	sub ebx, [esi + sqStack.base]
	shr ebx, 2 ;ebx = ebx / 4
	cmp ebx, [esi + sqStack.len]
	jnb .exitp
	mov eax, 0

.exitp:
	pop esi
	pop ebx
endproc

;int stackEmpty(struct sqStack *s)
;return: eax = 1 if empty; 0 if not empty
proc stackEmpty
;uses ebx esi,
%$s     arg

push ebx
push esi
	mov esi, [ebp + %$s]
	mov eax, 1
	;if (s->top == s->base)
	mov ebx, [esi + sqStack.top]
	cmp ebx, [esi + sqStack.base]
	je .exitp
	mov eax, 0

.exitp:
	pop esi
	pop ebx
endproc

;int pushdata(struct sqStack *s, int data)
;return: eax = 1 if fail; 0 if OK
proc pushdata
;uses esi ecx,

%$s     arg
%$data  arg

push ecx
push esi

	mov esi, [ebp + %$s]
	push esi
	call stackFull
	pop esi
	cmp eax, 1
	je .exitp ;stack is full
	
	;here eax is 0
	mov ecx, [esi + sqStack.top]
	push dword [ebp + %$data]
	pop dword [ecx]
	add dword [esi + sqStack.top], 4

.exitp:
	pop esi
	pop ecx
endproc

;int popdata(struct sqStack *s, int* data)
;return: eax = 1 if fail; 0 if OK
proc popdata
;uses ecx esi,

%$s     arg
%$data  arg

push ecx
push esi

	mov esi, [ebp + %$s]
	push esi
	call stackEmpty
	pop esi
	cmp eax, 1
	je .exitp ;stack is empty
	
	;here eax is 0
	sub dword [esi + sqStack.top], 4 ;top - 4
	mov ecx, [esi + sqStack.top]
	push dword [ecx]
	mov ecx, [ebp + %$data]
	pop dword [ecx]

.exitp:
	pop esi
	pop ecx
endproc


section .bss                                    ; Uninitialized data segment
alignb 8

	stackStru: resb sqStack_size
	array_stack: resw STACKLEN
	statckdata: resd 1

section .text

 proc testStack

	;init stack
	push STACKLEN
	push array_stack
	push stackStru
	call initStack
	add  esp, 12

	;test
	push stackStru
	call stackEmpty
	call stackFull
	add esp, 4
		
	mov ecx, STACKLEN + 1
	mov ebx, 0
.loop_push:
	inc ebx
	push ebx
	push stackStru
	call pushdata
	pop esi
	pop ebx

	cmp eax, 1
	je .next ;finished
	loop .loop_push
.next:

	;start pop
.loop_pop:
	push statckdata
	push stackStru
	call popdata
	add  esp, 8

	cmp eax, 1
	je .finish
	jmp .loop_pop

.finish:
    xor     eax,eax
endproc

