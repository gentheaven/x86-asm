
;--------- my own constants --------------
NULL EQU 0
TRUE EQU 1
FALSE EQU 0

STACKLEN equ 20
;--------- stack --------------
;struct sqStack {
;	int *top;  //sp
;	int *base; //ss
;	int len; //size count by itemStack
;}SqStack;

struc sqStack 
	.top: resd 1 ;sp
	.base: resd 1 ;ss
	.len: resd 1 ;count
endstruc

