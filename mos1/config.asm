;stack at 64KB - 0, 0x10000 - 0
;boot start at 0x7c00, size is less than 0x200 = 512B

;GDT config, max size is 0x64KB = 0x10000
gdt_base_addr equ 0x7e00 ;0x7e00 - 0x17e00

;kernel config
kernel_base_addr equ 0x00040000 ;start address
kernel_start_sector equ 1 ;start lba

;app config
app_base_addr equ 0x100000 ;start at 1MB

;-------------------------------------------------------------------------------

;gdt segment descripto
DA_LIMIT_1B equ 0
DA_LIMIT_4K	equ	0x80 ;G=1, 4K, Granularity
DA_DB equ 0x40 ;D = 1, 32bit
DA_L equ 0x20 ;L = 1, 64bit
DA_AVL equ 0x10 ;AVL, reserved
DA_LIM2_MASK equ 0xf

;R: Read only
;A: Accessed
;C: Code
;O: conforming
;E: E=1, stack; E=0, data
DA_DR		equ	0x90	;data, read only
DA_DRW		equ	0x92	;data, rw
DA_DRWA		equ	0x93	;data, rw, accessed
DA_DRWE		equ 0x96	;data, rw, E=1, stack
DA_C		equ	0x98	;code
DA_CR		equ	0x9A	;code, r
DA_CCO		equ	0x9C	;code, conforming
DA_CCOR		equ	0x9E	;code, conforming, r

;selector
SelAllData equ 0x8
SelCoreCode equ 0x10
SelStack equ 0x18
SelVgaData equ 0x20

sys_routine_seg_sel equ  0x28
core_data_seg_sel	equ  0x30
core_code_seg_sel   equ  0x38

;-------------------------------------------------------------------------------

;Descriptor(DA_LIMIT_4K, DA_type)
;default:  DA_DB = 1, L = 0, AVL = 0
;e.g. Descriptor(DA_LIMIT_4K, DA_DRW)
%define Descriptor(da_4k, type) \
	(((da_4k | DA_DB ) << 16) | (type << 8))

;DescBaseL(base, len)
;low 32b
;base: 32b; limit: 20b
;limit = len - 1
%define DescBaseL(base, len) \
	(((base & 0xffff) << 16) | ((len - 1) & 0xffff))

;DescBaseH(base, len)
;high 32b
%define DescBaseH(base, len) \
	(((base & 0x00ff0000) >> 16) | \
	(base & 0xff000000) | \
	(((len - 1) & 0xf0000)))

;G=1, DA_LIMIT_4K
;E=1, stack, extend to below
;input: stack len count by bytes
;output: limit + 1

;stack len = 0x1 0000 0000 - 4096 * (limit+1)
;= 4096 * (0x10 0000 - (limit + 1))
;limit + 1 = 0x10 0000 - len/4096
;e.g ,len = 4096 = 4KB, then
;limit + 1 = 0x100000 - 4096/4096 = 0xfffff
%define StackLen(len) \
	(0x100000 - len/4096)

;-------------------------------------------------------------------------------

GDT_SIZE equ 39 ;5 items, 5*8-1=39

;-------------------------------------------------------------------------------
;kernel head
;offset, see kernel.asm
KERNEL_HEAD_LEN		equ 0
KERNEL_HEAD_SYS		equ 0x4
KERENL_HEAD_DATA	equ 0x8
KERNEL_HEAD_CODE	equ 0xc
KERNEL_TOTAL_LEN	equ 0x10
KERNEL_HEAD_ENTRY	equ 0x14
