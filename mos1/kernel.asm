;in 32bit protect mode
;load app and run app

%include "config.asm"
core_length dd core_end
sys_routine_seg  dd section.sys_routine.start
core_data_seg    dd section.core_data.start
core_code_seg    dd section.core_code.start
code_size		 dd core_end
core_entry       dd start  ;kernel start address
	dw core_code_seg_sel

[bits 32]

;===============================================================================


;===============================================================================
SECTION sys_routine vstart=0

         ;字符串显示例程
put_string:                                 ;显示0终止的字符串并移动光标
                                            ;输入：DS:EBX=串地址
         push ecx
  .getc:
         mov cl,[ebx]
         or cl,cl
         jz .exit
         call put_char
         inc ebx
         jmp .getc

  .exit:
         pop ecx
         retf                               ;段间返回

;-------------------------------------------------------------------------------
put_char:                                   ;在当前光标处显示一个字符,并推进
                                            ;光标。仅用于段内调用
                                            ;输入：CL=字符ASCII码
         pushad

         ;以下取当前光标位置
         mov dx,0x3d4
         mov al,0x0e
         out dx,al
         inc dx                             ;0x3d5
         in al,dx                           ;高字
         mov ah,al

         dec dx                             ;0x3d4
         mov al,0x0f
         out dx,al
         inc dx                             ;0x3d5
         in al,dx                           ;低字
         mov bx,ax                          ;BX=代表光标位置的16位数

         cmp cl,0x0d                        ;回车符？
         jnz .put_0a
         mov ax,bx
         mov bl,80
         div bl
         mul bl
         mov bx,ax
         jmp .set_cursor

  .put_0a:
         cmp cl,0x0a                        ;换行符？
         jnz .put_other
         add bx,80
         jmp .roll_screen

  .put_other:                               ;正常显示字符
         push es
         mov eax, SelVgaData        ;0xb8000段的选择子
         mov es,eax
         shl bx,1
         mov [es:bx],cl
         pop es

         ;以下将光标位置推进一个字符
         shr bx,1
         inc bx

  .roll_screen:
         cmp bx,2000                        ;光标超出屏幕？滚屏
         jl .set_cursor

         push ds
         push es
         mov eax, SelVgaData
         mov ds,eax
         mov es,eax
         cld
         mov esi,0xa0                       ;小心！32位模式下movsb/w/d
         mov edi,0x00                       ;使用的是esi/edi/ecx
         mov ecx,1920
         rep movsd
         mov bx,3840                        ;清除屏幕最底一行
         mov ecx,80                         ;32位程序应该使用ECX
  .cls:
         mov word[es:bx],0x0720
         add bx,2
         loop .cls

         pop es
         pop ds

         mov bx,1920

  .set_cursor:
         mov dx,0x3d4
         mov al,0x0e
         out dx,al
         inc dx                             ;0x3d5
         mov al,bh
         out dx,al
         dec dx                             ;0x3d4
         mov al,0x0f
         out dx,al
         inc dx                             ;0x3d5
         mov al,bl
         out dx,al

         popad
         ret


;===============================================================================
SECTION core_data vstart=0

msg_krn_load	db  'kernel started', 0xd, 0xa, 0


;===============================================================================
SECTION core_code vstart=0

start:
	;init DS
	mov eax, core_data_seg_sel
	mov ds, eax

	;init stack
	mov eax, SelStack
	mov ss, eax
	mov esp, 0

	;show msg
	mov ebx, msg_krn_load
	call sys_routine_seg_sel:put_string

	jmp $

;===============================================================================
SECTION core_trail
core_end:



