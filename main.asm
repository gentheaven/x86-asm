.386                   ;enable instructions
        .xmm                    ;enable instructions
        .model flat,c           ;use C naming convention (stdcall is default)

;       include C libraries
        includelib      msvcrtd
        includelib      oldnames
        includelib      legacy_stdio_definitions.lib    ;for scanf, printf, ...

		include macros.inc
		include external.inc

		;======================================================

        .data                   ;initialized data
		.data?					;uninialized data



    .stack  4096            ;stack (optional, linker will default)

    .code                   ;code 
    public  asm_main

	;printf      PROTO C _format:DWORD,args:VARARG

asm_main    proc
		call testStack
        xor     eax,eax
        ret
asm_main    endp

        end