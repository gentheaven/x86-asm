
;printf related
  .data                   ;initialized data
printf_num db "%d ", 0
printf_enter db 0dh,0ah,0

mPutNum macro regName
;print this data
	push ecx
	push regName
	push offset printf_num
	call printf
	add sp, 8
	pop ecx
	
endm

mPutStr macro string
;print this data
	push ecx
	push offset string
	call printf
	add sp, 4
	pop ecx
endm

mPutCRLF macro
;printf crlf
	push ecx
	push offset printf_enter
	call printf
	add  sp,4
	pop ecx
endm
