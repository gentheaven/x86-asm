#include <stdio.h>
#include <math.h>

void print_array(unsigned int index, int* newrn, int* ary, unsigned int prec)
{
	unsigned int n;

	printf("%d:", index);
	for (n = 0; n < (prec - 1); n++) {
		printf("%d", newrn[n]);
	}
	printf("	%d.", ary[0]);
	for (n = 1; n < (prec - 2); n++) {
		printf("%d", ary[n]);
	}

	printf("%d", ary[n]);
	printf("\n");
}

//lg3+ log5/2 + ... + ... + lg(2k+1/k) > prec + 1
unsigned int computeTermN(unsigned int prec)
{
	unsigned int k = 0;
	double mid, sum;

	//printf("log2 = %f\n", log10f(2.0));
	sum = 0.0;
	for (k = 1; ; k++) {
		mid = (2 * k + 1) / k;
		mid = log10(mid);
		sum = sum + mid;
		if ((unsigned int)sum > (prec + 1))
			break;
	}
	//printf("prec is %d, loop times is %d\n", prec, k);
	return k;
}
