.386                   ;enable instructions
        .xmm                    ;enable instructions
        .model flat,c           ;use C naming convention (stdcall is default)

.code

;asm_memset(unsigned int *dest, unsigned int val, unsigned int len)
;len: bytes
asm_memset proc uses eax ebx ecx edi,
	dest: ptr dword, val: dword, len: dword

	mov ecx, len
	shr ecx, 2 ;count by dword
	mov edi, dest
	mov eax, 0 ;index
	mov ebx, val

setval:
	mov [edi + eax * 4], ebx
	inc eax
	loop setval

	ret
asm_memset endp

;memmove(unsigned int *dest, unsigned int *src, unsigned int len)
;len: bytes
asm_memmove proc uses eax ecx esi edi,
	dest: ptr dword, src: ptr dword, len: dword

	mov ecx, len
	shr ecx, 2 ;count by dword
	mov edi, dest
	mov esi, src
	mov eax, 0 ;index

move_loop:
	push [esi + eax * 4]
	pop [edi + eax * 4]
	inc eax
	loop move_loop

	ret
asm_memmove endp


;memcpy(unsigned int *dst, unsigned int *src, unsigned int len)
;len: bytes
memcpy proc uses ecx esi edi,
	dst: ptr dword, src: ptr dword, len: dword

	mov edi, dst
	mov esi, src
	mov ecx, 0
	shr len, 2

	cmp len, 0
	je exit

copy:
	mov eax, [esi + ecx * 4]
	mov [edi + ecx * 4], eax
	inc ecx
	cmp ecx, len
	jb copy

exit:
	ret
memcpy endp

end