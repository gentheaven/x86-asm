	;extrn   printf:near
printf PROTO C _format:DWORD,args:VARARG

	;void print_array(unsigned int index, int* newrn, int* ary,  unsigned int prec)
print_array proto C index: dword, newrn: ptr dword, ary: ptr dword, prec: dword
	
	;unsigned int computeTermN(unsigned int prec)
computeTermN proto C prec: dword

	;memcpy(unsigned int *dst, unsigned int *src, unsigned int len)
memcpy proto C dst: ptr dword, src: ptr dword, len: dword


	;asm_memset(unsigned int *dest, unsigned int val, unsigned int len)
asm_memset proto dest: ptr dword, val: dword, len: dword

	;memmove(unsigned int *dest, unsigned int *src, unsigned int len)
asm_memmove proto dest: ptr dword, src: ptr dword, len: dword
